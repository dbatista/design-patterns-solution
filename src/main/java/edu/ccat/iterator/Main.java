package edu.ccat.iterator;

import edu.ccat.iterator.cre.Cre;
import edu.ccat.iterator.facebook.Facebook;
import edu.ccat.iterator.google.Google;
import edu.ccat.iterator.core.Company;
import edu.ccat.iterator.core.UsersIterator;

import java.util.ArrayList;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
public class Main {

    public static void main(String[] args) {
        ArrayList<Company> companies = new ArrayList<Company>();
        companies.add(new Google());
        companies.add(new Facebook());
        companies.add(new Cre());

        for (Company company : companies) {
            showUsers(company.createIterator());
        }
    }

    public static void showUsers(UsersIterator iterator) {
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

}

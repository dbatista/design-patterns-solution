package edu.ccat.iterator.facebook;

import edu.ccat.iterator.core.UsersIterator;

import java.util.List;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
public class FacebookUsersIterator implements UsersIterator<FacebookUser> {

    private List<FacebookUser> users;
    private int position;

    public FacebookUsersIterator(List<FacebookUser> users) {
        this.users = users;
        this.position = 0;
    }

    public FacebookUser next() {
        return users.get(position++);
    }

    public boolean hasNext() {
        return position < users.size();
    }

    public FacebookUser currentUser() {
        return users.get(position);
    }
}

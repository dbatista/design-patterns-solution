package edu.ccat.iterator.facebook;

import edu.ccat.iterator.core.Company;
import edu.ccat.iterator.core.UsersIterator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
public class Facebook implements Company {

    public List<FacebookUser> getFacebook() {
        ArrayList<FacebookUser> users = new ArrayList<FacebookUser>();

        users.add(new FacebookUser("Shigeo T.", "https://fb.com/user/shigeo"));
        users.add(new FacebookUser("MArioa", "https://fb.com/user/laPropia"));
        users.add(new FacebookUser("Roberto", "https://fb.com/user/beto_87"));
        users.add(new FacebookUser("Jaime", "https://fb.com/user/james"));

        return users;
    }

    public UsersIterator createIterator() {
        return new FacebookUsersIterator(getFacebook());
    }
}

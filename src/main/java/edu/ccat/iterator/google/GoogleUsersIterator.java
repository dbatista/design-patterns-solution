package edu.ccat.iterator.google;

import edu.ccat.iterator.core.UsersIterator;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
public class GoogleUsersIterator implements UsersIterator<GoogleUser> {

    private GoogleUser[] users;
    private int position;

    public GoogleUsersIterator(GoogleUser[] users) {
        this.users = users;
        this.position = 0;
    }

    public GoogleUser next() {
        return users[position++];
    }

    public boolean hasNext() {
        return position < users.length;
    }

    public GoogleUser currentUser() {
        return users[position];
    }
}

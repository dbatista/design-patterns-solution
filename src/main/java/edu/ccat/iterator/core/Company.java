package edu.ccat.iterator.core;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
// Este es el Agreegate
public interface Company {

    UsersIterator createIterator();
}

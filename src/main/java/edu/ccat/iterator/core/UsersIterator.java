package edu.ccat.iterator.core;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
public interface UsersIterator<T> {

    T next();
    boolean hasNext(); //isDone()
    T currentUser();
}

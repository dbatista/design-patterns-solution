package edu.ccat.iterator.cre;

import edu.ccat.iterator.core.Company;
import edu.ccat.iterator.core.UsersIterator;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by David on 10/11/2016.
 */
public class Cre implements Company {
    public Map<Long, CreUser>  getCREinfo(){
        Map<Long, CreUser> users = new HashMap<Long, CreUser>();
        users.put((long) 1, new CreUser(1, new UserData("Juan Perez", "Villa rosita", 100.50)));
        users.put((long) 2, new CreUser(2, new UserData("Maria Ruiz", "Villa 1ro de Mayo", 500.78)));
        return users;
    }

    public UsersIterator createIterator() {
        return new CreUsersIterator(this.getCREinfo());
    }
}

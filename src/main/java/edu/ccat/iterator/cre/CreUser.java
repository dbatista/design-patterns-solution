package edu.ccat.iterator.cre;

/**
 * Created by David on 10/11/2016.
 */
public class CreUser {
    private int id;
    private UserData userData;

    public CreUser(int id, UserData userData) {
        this.id = id;
        this.userData = userData;
    }

    public int getId() {
        return id;
    }

    public UserData getUserData() {
        return userData;
    }

    @Override
    public String toString(){
        return String.format("CreUser: %s, Address: %s, Payment: %s", this.userData.getName(), this.userData.getAddress(), this.userData.getPayment());
    }
}

package edu.ccat.iterator.cre;

import edu.ccat.iterator.core.UsersIterator;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by David on 10/11/2016.
 */
public class CreUsersIterator implements UsersIterator<CreUser> {
    private ArrayList<CreUser> users;
    private int position;

    public CreUsersIterator(Map<Long, CreUser> users) {
        this.users = new ArrayList<CreUser>(users.values());
        position = 0;
    }

    public CreUser next() {
        return users.get(position++);
    }

    public boolean hasNext() {
        return position < users.size();
    }

    public CreUser currentUser() {
        return users.get(1);
    }
}

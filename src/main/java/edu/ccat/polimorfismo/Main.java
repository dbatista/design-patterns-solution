package edu.ccat.polimorfismo;

import javax.swing.*;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
public class Main {

    public static void main(String[] args) {
        Icon redBox = new BoxIcon();

        JOptionPane.showMessageDialog(null,
                "Cuidado ya es hora",
                "Advertencia",
                JOptionPane.WARNING_MESSAGE,
                redBox);
    }
}
